package DBUtils;

import java.sql.*;
import java.util.logging.Logger;

public class DBConnection {

	private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());

	private static Connection createConnection() {

		Connection connection = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection(DBUtil.URL, DBUtil.USER_NAME, DBUtil.PASSWORD);
			if (connection != null) {
				LOGGER.info("Connection Successful :)");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static Connection getConnection() {
		return createConnection();
	}
	
}
