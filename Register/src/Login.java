import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import dao.LoginDao;
import delegates.Delegate;

import java.sql.*;

public class Login extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();

		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		
		
		if (LoginDao.checkUser(email, pass)) {
			out.println("<script>alert('You have successfully logged in')</script>");
			Cookie cookie = new Cookie("sessionToken", email);
			response.addCookie(cookie);
			cookie.getValue();
			RequestDispatcher rd = request.getRequestDispatcher("/screens/home.html");
			rd.forward(request, response);
		} else {
			out.println("<script>alert('You have entered invalid credentials')</script>");
			RequestDispatcher rd = request.getRequestDispatcher("index.html");
			rd.include(request, response);
		}
	}
}