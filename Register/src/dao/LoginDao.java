package dao;

import DBUtils.DBConnection;
import models.RegisterModel;
import queries.Query;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDao {

	private static Connection connection = DBConnection.getConnection();
	private static PreparedStatement preparedStatement = null;

	public static boolean registerStudent(RegisterModel register) {
		
		int i = 0;
		
    		try {
				preparedStatement = connection.prepareStatement(Query.REGISTER_QUERY);
				preparedStatement.setString(1, register.getName());
	    		preparedStatement.setString(2, register.getEmail());
	    		preparedStatement.setString(3, register.getPass());
				i = preparedStatement.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		if(i > 0) {
				return true;
			}else {
				return false;
			}
	}
	
	public static boolean checkUser(String email, String pass) {
		boolean statement = false;
		try {
			preparedStatement = connection.prepareStatement(Query.LOGIN_QUERY);
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, pass);
			ResultSet resultSet = preparedStatement.executeQuery();
			statement = resultSet.next();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return statement;
	}	
}
