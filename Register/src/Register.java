import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import dao.LoginDao;
import delegates.Delegate;
import models.RegisterModel;

import java.sql.*;

public class Register extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();

		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		
		RegisterModel register = new RegisterModel();
		if(Delegate.emailValidator(email)) {
			register.setEmail(email);
		}else {
			out.println("<script>alert('Enter a valid email address :|')</script>");
		}
		register.setName(name);
		register.setPass(pass);
		boolean result = LoginDao.registerStudent(register);
				
		if(result) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/login.html");
			dispatcher.forward(request, response);
		}else {
			out.println("<script>alert('An error has encountered during registration :(')</script>");
		}

	}
	
}