package delegates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Delegate {

	private static final Pattern VALID_EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	
	private static Matcher matcher;

	public static boolean emailValidator(String email) {
		matcher = VALID_EMAIL_REGEX.matcher(email);
		return matcher.find();
	}

}
