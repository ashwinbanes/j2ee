import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class Home extends HttpServlet {

   protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String htmlContents = "<html>\r\n" + 
        		"<body style=\"display: flex; align-items: center;\r\n" + 
        		"justify-content: center; font-family: Arial; width: 98%; height: 500px\">\r\n" + 
        		"<section>\r\n" + 
        		"  <h1>\r\n" + 
        		"  Logged in Successfully :)!!\r\n" + 
        		"  </h1>\r\n" + 
        		"</section>\r\n" + 
        		"</body>\r\n" + 
        		"</html>";
        try {
            out.println(htmlContents);
        } finally {            
            out.close();
        }
    }
}